#include <stdio.h>
#include <stdint.h>

int main(void) {

  printf("int is this big in bits: %lu\n", 8 * sizeof(int));
  printf("char is this big in bits: %lu\n", 8 * sizeof(char));
  printf("long is this big in bits: %lu\n", 8 * sizeof(long));
  printf("long long is this big in bits: %lu\n", 8 * sizeof(long long));

  return 0;
}
