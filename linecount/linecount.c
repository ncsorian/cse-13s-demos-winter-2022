#include <stdio.h>

int main(void) {
  int c = 0;
  int lines = 0;
  int vowels = 0;


  while ((c = getchar()) != EOF) {
    putchar(c);

    if (c == '\n') {
      lines++;
    } else if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'
               || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') {
      vowels++;
    }
  }

  printf("I counted this many lines: %d\n", lines);
  printf("I counted this many vowels: %d\n", vowels);
  return 0;
}
