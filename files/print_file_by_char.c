#include <stdio.h>

int main(void) {
  FILE* infile;
  int c;

  infile = fopen("print_file.c", "r");

  while(1) {
    c = fgetc(infile);
    if (c == EOF) {
      break;
    }
    putchar(c);
  }

  fclose(infile);
  return 0;
}
