#include <stdio.h>

int main(int argc, char **argv) {
  FILE* infile;
  char buf[1024];

  printf("my name is %s\n", argv[0]);
  for(int i=0; i < argc; i++) {
    printf("argv[%d] = %s\n", i, argv[i]);
  }


  if (argc >= 2) {
    infile = fopen(argv[1], "r");
  } else {
    infile = fopen("print_file.c", "r");
  }

  while(fgets(buf, 1024, infile) != NULL) {
    printf("%s", buf);
  }

  fclose(infile);
  return 0;
}
