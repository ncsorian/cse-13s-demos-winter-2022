#include <stdio.h>

int add_numbers(int a, int b) {
  return (a + b);
}

// function prototype
void swap(int *a, int *b);

int main(void) {
  int c;
  int first = 27;
  int second = 100;
  
  c = add_numbers(first, second);

  printf("%d\n", c);

  printf("first = %d, second = %d\n", first, second);
  swap(&first, &second);
  printf("first = %d, second = %d\n", first, second);

  return 0;
}


void swap(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}
