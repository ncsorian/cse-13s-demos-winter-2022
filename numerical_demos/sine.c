#include <stdio.h>
#include <math.h>

int main(void) {
  double nearly_pi = 3.1415;


  double sin_of_pi = sin(nearly_pi);
  printf("%f\n", sin_of_pi);

  return 0;
}
