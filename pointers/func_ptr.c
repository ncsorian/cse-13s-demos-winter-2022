#include <stdio.h>

int add_three(int x) {
  return x + 3;
}

int call_pointer_on(int (*funk)(int), int value) {
  return funk(value);
}

int main(void) {

  // f is a pointer to a function that takes an int and returns an int.
  int (*f)(int) = add_three;
  // f = add_three;

  printf("what's that address? %p\n", f);

  int number = f(105);
  printf("here is the result: %d\n", number);

  printf("let's get to this shortly: %d\n", call_pointer_on(f, 12));

  return 0;
}
