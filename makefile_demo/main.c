#include <stdio.h>
#include "factorial.h"

int main(void) {
  int result = factorial(10);

  printf("I did some computation with *RECURSION* and I got: %d\n", result);
  return 0;
}
