#include <stdio.h>
#include <stdlib.h>

long long fib(int k) {
  if (k == 0 || k == 1) {
    return k;
  }
  long long prev = 1;
  long long prevprev = 0;
  long long cur;

  for (int i = 2; i <= k; i++) {
    cur = prevprev + prev;
    prevprev = prev;
    prev = cur;
  }
  return cur;
}

int main(int argc, char **argv) {
  int x;

  if (argc < 2) {
    x = 10;
  } else {
    x = strtol(argv[1], NULL, 10);
  }
  printf("%lld\n", fib(x));

  return 0;
}
