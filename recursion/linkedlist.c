#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// linked list nodes
typedef struct Node Node;

struct Node {
  // first, car
  int data;
  // rest, cdr
  Node *next;
};

Node *add_node_to_front(int item, Node* rest) {
  Node *newnode = (Node *)calloc(1, sizeof(Node));
  newnode->data = item;
  newnode->next = rest;
  return newnode;
}

void walk_list(Node *list) {
  if (list == NULL) {
    return;
  }
  printf("I saw: %d\n", list->data);
  walk_list(list->next);
}

bool search_list(Node *list, int value) {
  if (list == NULL) {
    return false;
  }
  printf("I saw: %d\n", list->data);

  if (list->data == value) {
    return true;
  } else {
    return search_list(list->next, value);
  }
}

int main(void) {

  Node *front = NULL;
  front = add_node_to_front(1337, front);
  front = add_node_to_front(420, front);
  front = add_node_to_front(21, front);
  front = add_node_to_front(10, front);
  front = add_node_to_front(25, front);

  bool found = search_list(front, 420);
  printf("did I find it? %d\n", found);

  found = search_list(front, 421);
  printf("did I find it? %d\n", found);

  return 0;
}
