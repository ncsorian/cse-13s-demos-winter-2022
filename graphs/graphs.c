#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
  int vertices;  // the set V
  int **matrix;  // the set E
} Graph;

typedef struct LLint {
  int val;
  struct LLint *next;
} LLint;

LLint *add_to_set(LLint *set, int val) {
  LLint *newfront = calloc(1, sizeof(LLint));
  newfront->val = val;
  newfront->next = set;

  return newfront;
}

bool set_contains(LLint *set, int val) {
  if (set == NULL) {
    return false;
  } else if (set->val == val) {
    return true;
  } else {
    return set_contains(set->next, val);
  }
}

LLint *enqueue(LLint *q, int val) {
  LLint *newnode = calloc(1, sizeof(LLint));
  newnode->val = val;

  if (q == NULL) {
    return newnode;
  }

  LLint *cur = q;
  while(cur->next != NULL) {
    cur = cur->next;
  }
  cur->next = newnode;
  return q;
}

bool dequeue(LLint **q, int *ret) {
  if (*q == NULL) {
    return false;
  }

  *ret = (*q)->val;
  
  LLint *freethis = *q;
  *q = (*q)->next;
  free(freethis);
  return true;
}

Graph *graph_create(int vertices) {
  Graph *g = (Graph *)malloc(sizeof(Graph));
  g->vertices = vertices;
  g->matrix = (int **)calloc(vertices, sizeof(int*));
  for (int i=0; i < vertices; i++) {
    g->matrix[i] = (int *)calloc(vertices, sizeof(int));
  }
  return g;
}

void graph_add_edge(Graph *g, int i, int j) {
  g->matrix[i][j] = 1;
}


bool graph_has_edge(Graph *g, int i, int j) {
  return g->matrix[i][j];
}


bool graph_has_path_bfs(Graph *g, int i, int j) {
  /*
    keep track of a set of places that we have visited
    keep a queue of places that we want to visit
    while I have more places to visit:
       get the next place to visit
       see if it's where we're going (= j)
       if it's not, enqueue all of its neighbors that have not been visited
    return false if we're out of places
   */

  LLint *visited = NULL;
  LLint *to_visit = NULL;

  to_visit = enqueue(to_visit, i);

  while(to_visit != NULL) {
    int current;
    dequeue(&to_visit, &current);

    if (current == j) {
      return true;
    }

    visited = add_to_set(visited, current);

    // find all of the neighbors of the current node
    for(int neighbor = 0; neighbor < g->vertices; neighbor++) {
      if (graph_has_edge(g, current, neighbor) &&
          !set_contains(visited, neighbor)) {
        to_visit = enqueue(to_visit, neighbor);
      }
    }
  }
  return false;
}

int main(void) {
  Graph *g = graph_create(10);

  graph_add_edge(g, 0, 1);
  graph_add_edge(g, 1, 7);
  graph_add_edge(g, 7, 2);
  graph_add_edge(g, 7, 3);
  graph_add_edge(g, 4, 5);

  bool path_exists;
  path_exists = graph_has_path_bfs(g, 0, 3);
  printf("path exists from 0 to 3? %d\n", path_exists);

  path_exists = graph_has_path_bfs(g, 4, 5);
  printf("path exists from 4 to 5? %d\n", path_exists);

  path_exists = graph_has_path_bfs(g, 3, 6);
  printf("path exists from 3 to 6? %d\n", path_exists);

  path_exists = graph_has_path_bfs(g, 2, 3);
  printf("path exists from 2 to 3? %d\n", path_exists);

  return 0;
}
