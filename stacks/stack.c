#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "stack.h"

Stack *stack_create(void) {
  Stack *out;

  out = (Stack *)calloc(1, sizeof(Stack));
  return out;
}

bool stack_push(Stack *s, int item) {
  Node *newtop = (Node *)calloc(1, sizeof(Node));
  if (newtop == NULL) {
    return false;
  }

  newtop->data = item;
  newtop->next = s->top;

  s->top = newtop;
  return true;
}

bool stack_pop(Stack *s, int *val) {
  if (s == NULL || s->top == NULL) {
    return false;
  }

  int result = s->top->data;
  *val = result;

  Node *delete_this = s->top;
  s->top = s->top->next;

  free(delete_this);
  return true;
}

int main(void) {

  Stack *mystack;
  mystack = stack_create();

  stack_push(mystack, 10);
  stack_push(mystack, 20);
  stack_push(mystack, 30);
  stack_push(mystack, 100);
  stack_push(mystack, 42);
  stack_push(mystack, 420);

  int result;
  while(stack_pop(mystack, &result)) {
    printf("got this number: %d\n", result);
  }

  return 0;
}
