#pragma once

#include <stdbool.h>

// linked list nodes
typedef struct Node Node;

struct Node {
  Node *next;
  int data;
};

typedef struct {
  Node *top;
} Stack;

Stack *stack_create(void);

void stack_delete(Stack **s);

void stack_empty(Stack *s);

bool stack_push(Stack *s, int item);

bool stack_pop(Stack *s, int *val);
