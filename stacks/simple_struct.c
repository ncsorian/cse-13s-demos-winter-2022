#include <stdio.h>
#include <stdbool.h>
#include <string.h>

typedef struct {
  char name[255];
  bool has_taken_thirteen_s;
} Student;


int main(void) {
  Student sally;

  sally.name[0] = '\0';
  strncat(sally.name, "Sally", 255);
  sally.has_taken_thirteen_s = true;


  printf("sally's name: %s\n", sally.name);


  return 0;
}
